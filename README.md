# THE BACCO EMULATORS

baccoemu provides a collection of emulators for large-scale structure statistics over a wide range of
cosmologies.

## Installation

Follow detailed instructions at https://baccoemu.readthedocs.io/en/latest/#installation

Anyway, a quick installation should boil down to

```bash
pip install . [--user]
```

## Usage

Follow detailed instructions at https://baccoemu.readthedocs.io/en/latest/#quickstart

You can also see an example in this same folder in the file example.py

## Credit our work

If you use baccoemu, please cite our papers:

- the `linear cold- and total-matter power spectrum <https://arxiv.org/abs/2104.14568>`_
- the `nonlinear cold-matter power spectrum <https://arxiv.org/abs/2004.06245>`_
- the `modifications to the cold-matter power spectrum caused by baryonic physics <https://arxiv.org/abs/2011.15018>`_

For more details, visit our `our website <http://bacco.dipc.org>`_

## License
[MIT](https://choosealicense.com/licenses/mit/)
